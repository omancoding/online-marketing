var mongoose = require ('mongoose');

var uristring = process.env.MONGODB_URI

mongoose.connect(uristring, function (err, res) {
  if (err) { 
    console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + uristring);
  }
});

var itemSchema = new mongoose.Schema({
	title: { type: String, trim: true },
	description: { type: String},
	link: { type: String/*, unique: true*/},
	imgLink: { type: String/*, unique: true*/},
	price: { type: Number/*, unique: true*/},
  category: {type: String}
});
var userSchema = new mongoose.Schema({
  name: { type: String, trim: true },
  id: { type: String, unique: true},
  password: { type: String},
  role: {type: String},
  other: {}
});
var orderSchema = new mongoose.Schema({
  id: { type: String, unique: true},
  idx: {type: Number, unique: true},
  order : { type: String},
  status: {type: String},
  maker: {type: String},
  dperiod: {type: String},
  timestamp : { type: Date, default: Date.now },
});

var configureSchema = new mongoose.Schema({
  orderidx: {type: Number, default:4000}
})

var ItemModel = mongoose.model('items', itemSchema);
var UserModel = mongoose.model('musers', userSchema);
var OrderModel = mongoose.model('orders', orderSchema);
var ConfigureModel = mongoose.model('configurations', configureSchema);


ConfigureModel.findOne({}, function(error, item) {
  console.log('before', item);
  if (item == null) { // Array has no element
    config = new ConfigureModel();
    config.save(function (err) {if (err) console.log ('Error on save!', err)});
  }
})

module.exports =  { 'object': mongoose, 
                    'models': { 
                      'items': ItemModel,
                      'users': UserModel,
                      'orders': OrderModel,
                      'config': ConfigureModel
                    },
                  }
