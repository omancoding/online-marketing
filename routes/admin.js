// 
// It will work in sha ALLAH
//
var express = require('express');
var router = express.Router();
var models = require('./data/mongo.js').models;

//seed();

var messages = 
{
	'1': 'تم تعديل البيانات'
}

var errors = 
{

}

router.use(function(req,res,next) {
	var msg = req.session.msg;
	req.session.msg = null;
	var notif = null;

	if (msg) {
		req.notif = messages[msg]
	}
	next();
})

router.get('/orders', function(req, res, next) {
	models.orders.find({status:'init'}, function(err, orders) {
		res.render('admin/orders', {orders: orders, notif:req.notif});
	})

});

router.get('/change/:type', function(req, res, next) {

	models.items.find({}, function(err, items) {
		//console.log(items);
		models.orders.find({}, function(err, orders) {
			res.render('admin/main', {items: items, notif:req.notif});
		})
	})
});

router.post('/changes/:type', function(req, res, next) {
	var changes = JSON.parse(req.body.changes);
	for (var r in changes) {
		var oupdate= update.bind(null, r);
		oupdate();
	}
	req.session.msg = 1;
	res.redirect('/admin/change/'+req.params.fruits);

	function update(r) {
		models.items.findById(r, function(e, item) {
			if (e) {
				console.log(e);
				return;
			}
			item.price = changes[r];
			item.save(function(e, uitem) {
				if (e) {
					console.log(e);
				}
			})
		})
	} 
});

router.post('/delete/:id', function(req, res, next) {

	models.items.find({}, function(err, items) {
		//console.log(items);
		models.orders.find({}, function(err, orders) {
			res.render('admin/main', {items: items, notif:req.notif});
		})
	})
});

router.get('/api/action/:action/:id', function(req, res, next) {
	var id = req.params.id;
	var action = req.params.action;

	models.orders.findOne({id:id}, function(err, item) {
		item.status = action;
		item.save(function(e, uitem) {
			if (e) {
				console.log(e);
			}
		})
	})
});


function seed() {

	// Clear out old data
	models.users.remove({}, function(err) {
	  if (err) {
	    console.log ('error deleting old data.');
	  }
	});

	// Creating one user.
	var user1 = new models.users ({
	  name: 'user1',
	  id: '1000',
	  password: 'easy',
	  role: 'admin'
	});

	// Saving it to the database.  
	user1.save(function (err) {if (err) console.log ('Error on save!', err)});
	
	// Creating one user.
	var user2 = new models.users ({
	  name: 'user2',
	  id: '2000',
	  password: 'easy',
	  role: 'admin'
	});

	// Saving it to the database.  
	user2.save(function (err) {if (err) console.log ('Error on save!', err)});

}

module.exports = router;
