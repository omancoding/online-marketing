var express = require('express');
var router = express.Router();
var passport = require('passport');
var uuidv4 = require('uuid/v4');
var models = require('./data/mongo.js').models;


var messages = 
{
  '1': 'تم الطلب بنجاح'
}

/* GET home page. */
router.get('/', function(req, res, next) {

  var msg = req.session.msg;
  req.session.msg = null;
  var notif = null;
  if (msg) {
    notif = messages[msg]
  }

  var mobile = process.env == 'production' ? '' : '94033945';

	models.items.find({}, function(err, items) {
		//console.log(items);
		res.render('index', {items: items, notif:notif, mobile:mobile});
	})
});

router.post('/order', function(req, res, next) {
  var timestamp = new Date();
  var uid = uuidv4();
  var status = 'init';

  models.config.findOneAndUpdate({}, { $inc: { orderidx: 1 }}, function(err, config) {
    //console.log(config);
    order(config.orderidx)
  });


  function order(num) {
    var order = new models.orders ({
      id: uid,
      idx: num,
      status: status,
      order: req.body.order,
      maker: req.body.mobile,
      dperiod: req.body.dperiod
    });
    order.save(function (err) {
      if (err) return console.log ('Error on save!', err);
      req.session.msg = 1;
      res.redirect('/');    
    });
  }
  
});

router.get('/test', function(req, res, next) {
  for (var i in [1,2,3,4]) {
    setTimeout(function(num) {
      console.log('num:',num)
    }.bind(null,i),3000*i)
  }
  res.send('ok')
});

var kue = require('kue')
 , queue = kue.createQueue({redis: process.env.REDIS_URL });

queue.on( 'error', function( err ) {
  console.log( 'Oops... ', err );
});

queue.on('job enqueue', function(id, type){
  console.log( 'Job %s got queued of type %s', id, type );

}).on('job complete', function(id, result){
  kue.Job.get(id, function(err, job){
    if (err) return;
    job.remove(function(err){
      if (err) throw err;
      console.log('removed completed job #%d', job.id);
    });
  });
});



router.post('/add', function(req, res, next) {
	var data = req.body;

	queue.create('scrape', {data: data}).save();
   res.redirect('/')

});

router.get('/sign-s3', /*invidChecker,*/  function(req, res, next) {
  const s3 = new aws.S3();
  //const fileName = req.query['file-name'];
  const fileName = uuidv4();
  const fileType = req.query['file-type'];
  const s3Params = {
    Bucket: S3_BUCKET,
    Key: fileName,
    //CacheControl: 'max-age=3456000, public', // doesn't work in Chrome, FF & Safari do return 304 but not as shown here
    ContentType: fileType,
    ACL: 'public-read',

  };

  s3.getSignedUrl('putObject', s3Params, (err, data) => {
    if(err){
      console.log(err);
      return res.end();
    }
    const returnData = {
      signedRequest: data,
      url: `https://${S3_BUCKET}.s3.amazonaws.com/${fileName}`,
      fileName: fileName
    };
    res.write(JSON.stringify(returnData));
    res.end();
  });
});

// [Authentication] 
router.get('/login', function(req, res, next) {
  res.render('admin/login');
});

router.post('/login', 
  passport.authenticate('local', { failureRedirect: '/login' }),
  function(req, res) {
    console.log(req.body);
    res.redirect('/admin/orders');
});



// [End Authentication] 


module.exports = router;
