var express = require('express');
require('dotenv').config();
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var uuidv4 = require('uuid/v4');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var moment = require('moment');
moment.locale('ar');

var models = require('./routes/data/mongo.js').models;
var mongoose = require('./routes/data/mongo.js').object;


var routes = require('./routes/index');
var users = require('./routes/users');
var admin = require('./routes/admin');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');



var time = 3600000 * 24 * 120; // 120 days;
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('express-session')({
  cookie: {  /* httpOnly:false, expires: new Date(Date.now() + time)*/ maxAge: time },
  genid: function(req) { var uid = uuidv4(); return uid; /* use UUIDs for session IDs*/},
  secret: '9876543210', 
  name:'user', 
  resave: false, 
  saveUninitialized: false,
  store: new MongoStore({ mongooseConnection: mongoose.connection })
 }));

app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next) {

  /*console.log('Age:',req.session.cookie.maxAge )
  console.log('Expires:',req.session.cookie.expires )
  console.log('Session:',req.session )*/
  res.locals.moment = moment;
  res.locals.user = req.user;
  next();
});

app.use('/', routes);
app.use('/users', users);
app.use('/admin',permit('admin'), admin);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



passport.use(new Strategy(
  function(username, password, cb) {
  models.users.findOne({ id: username}, function (err, user){
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      if (user.password != password) { return cb(null, false); }
      return cb(null, user);
    });
  }));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  models.users.findOne({ id: id}, function (err, user){
    if (err) { return cb(err); }
    //console.log(user)
    cb(null, user);
  });
});

function permit() {
  for (var _len = arguments.length, allowed = Array(_len), _key = 0; _key < _len; _key++) {
    allowed[_key] = arguments[_key];
  }

  var isAllowed = function isAllowed(role) {
    return allowed.indexOf(role) > -1;
  };

  // return a middleware
  return function (req, res, next) {
    if (req.user && isAllowed(req.user.role)) next(); // role is allowed, so continue on the next middleware
    else {
        console.log('here')
        res.redirect('/login');
    }
  };
}




module.exports = app;
