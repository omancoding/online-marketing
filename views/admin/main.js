var changes = {};
$(document).ready(function() {
    $("input").focus(function() { $(this).select(); } );
    $("input").change(function() { 
    	// compare with original value
    	var value = $(this).val();
    	var originalvalue = $(this).attr('originalvalue');
    	var id = $(this).attr('id');
    	delete changes[id];
    	if (originalvalue !== value) {
    		changes[id] = value;
    		$('#changes').val(JSON.stringify(changes));
    	}
		//console.log(changes)
    } );

});