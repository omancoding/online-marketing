$(document).ready(function() {

    $("#menu").mmenu({
      "navbar": {title: 'الإدارة'},
       "extensions": [
          "fx-menu-zoom",
          "fx-panels-zoom",
          "pagedim-black",
          "border-full"
       ],
       "rtl" : {use: true},
       "counters": true,
       "searchfield" : { placeholder: 'البحث'},
       "navbars": [
          {
             "position": "top",
             "content": [
                "searchfield"
             ]
          },
          {
             "position": "bottom",
             "content": [
                "<a class='fa fa-envelope' href='#/'></a>",
                "<a class='fa fa-twitter' href='#/'></a>",
                "<a class='fa fa-facebook' href='#/'></a>"
             ]
          }
       ]
    });

    //var notyf = new Notyf({delay: 5000});
    //notyf.confirm('hala');        
    //notyf.confirm('test');        
});

// One object if to be stacked.
var notyf = new Notyf({delay: 5000});

function buttonOrders(type,id,elem) {
  //alert(type, id);
  var delmsg = 'هل تريد أن قوم بإلغاء هذا الطلب';
  var commsg = 'هل تريد أن تقوم بتغيير حالة الطلب إلى "تم التسليم"';
  var msg = 'لقد تمت عملية الإلغاء';
  var msg1 = 'لقد تمت عملية تغيير الحالة بنجاح';

  var r;
  if (type == 'delete') {
    r = confirm(delmsg);
    console.log('r', r)
    if (r) {
      asyncReq('/admin/api/action/delete/'+id);
      notyf.confirm(msg);  
    }
  } else if (type == 'complete') {
    r = confirm(commsg);
    if (r) {
      asyncReq('/admin/api/action/complete/'+id);
      notyf.confirm(msg1);
    }
  }
  //console.log($(elem).closest('.oitem'));

  if (r) {
    $(elem).closest('.oitem').toggle('slide');
  }
  function asyncReq(path) {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          //Nothing
        }
      };
      xhttp.open("GET", path , true);
      xhttp.send();
  }
}

