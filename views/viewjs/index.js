var order = {};
var total = 0;

$('.add').click(function() {
  var id = $(this).attr('id');
  var price = $(this).attr('price');
  var title = $(this).attr('title');		

  addOne(id, price, title);
});

function addOne(item, price, title) {
	order[item] = order[item] || {};
	var count = order[item].count = order[item].count + 1 || 1;
	order[item].price = price;
	order[item].title = title;
	updateBadge(item, count);
	updateTotal();
}

function updateBadge(item, count) {
	var badge = $('.item[id='+item+'] .badge').length ? $('.item[id='+item+'] .badge') : $('.item[id='+item+']').append('<div class="badge" onclick="minusOne(\''+item+'\')"> </div>').children("div:last-child")
	count == 0 ? badge.remove() : badge.html(count);	
}

function minusOne(item) {
	var count = order[item].count = order[item].count - 1;
	updateBadge(item, count);
	updateTotal();
}

function updateTotal() {
	total = 0;
	$('.summary').empty()
	for (var i in order) {
		total = total + (order[i].count * order[i].price);
		$('.summary').append('<span style="flex:70%"> '+ order[i].title +'</span>');
		$('.summary').append('<span style="flex:30%"> '+ (order[i].count * order[i].price).toFixed(3) +'</span>');
	}
	total = total.toFixed(3);
	$('.summary').append('<span style="flex:70%"> الإجمالي </span>');
	$('.summary').append('<span style="flex:30%; background:lightgrey;">'+ total +' ريال </span>');
	$('#orderinput').val(JSON.stringify({order: order, total: total}))

	//$('.total').text(' ('+total+' ريال)');
	//console.log(total);
}

$('.order').click(function() {
    $('html, body').animate({
        scrollTop: $(".summary").offset().top
    }, 1000);
});

$('.location').click(function() {
  getLocation();
});

$('#homefront').on('change', function() {
	//alert($(this).val());
	updateImageDisplay(this)
})

function updateImageDisplay(obj) {
	var file = obj.files[0];
	
	var img = $('<img width="100%" > </img');
	//alert(file)
	var reader = new FileReader();
    reader.onload = function(e) {
		img.attr('src', e.target.result)
    };
    reader.readAsDataURL(file);

	$('.attachments').after(img)
}

var x = document.getElementById('geolocation');

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude;
}

$('#userorder').submit(function() {
	var notyf = new Notyf({delay: 5000});

	if (total < 5) {
		notyf.alert('أقل طلب هو ٥ ريالات');
		return false;	
	}

	var mobilenumber = $('#mobnum').val();
	if (mobilenumber.length < 7 || mobilenumber.length > 8) {
		notyf.alert('الرجاء التأكد من رقم الهاتف');
		return false;	
	}

	return true;
	
});

