const rp = require('request-promise');
const cheerio = require('cheerio');

const options = {
  uri: `https://www.luluwebstore.com/products/lulu-fresh-food-fresh-meat--fish-fresh-fish/fresh/fresh-tilapia--1kg-approx-weight/pid-8392547.aspx`,
  transform: function (body) {
    return cheerio.load(body);
  }
};

rp(options)
  .then(($) => {
    //console.log($);
    console.log($('#lblOfferPrice .sp_amt').text());
    console.log($('#bankImage').attr('src'))
  })
  .catch((err) => {
    console.log(err);
  });
  