require('dotenv').config()

var kue = require('kue')
 , queue = kue.createQueue({redis: process.env.REDIS_URL});



console.log('Worker Process Started');

queue.process('scrape', function(job, done){
  scrape(job.data.data, done);
});

function scrape(data,done) {
	const options = {
		uri: data.link,
		headers: {
      		'Cookie':'ASP.NET_SessionId=oraj1ikl3lh1lgroalcfgqb5; _ga=GA1.2.1368182382.1520267922; _gid=GA1.2.1866741639.1520267922; cto_lwid=d184daea-4286-4be6-b9df-03d0da95820c; ci_id=443b3fcc-7690-423d-9663v2-t161f707974c-49c30d26bcb6; __zlcmid=lHhAW36vAilizJ; __zlcprivacy=1; Site_Config={"LocationId":"13236","DeliverySlot":"","DM":"H"}; SERVERID=lu-web-01; CurrencyCode=OMR; antiForgeryToken=f16a7646-9dfa-4257-ab20-1d1c6f23118b; inptime0_6365_en=0; _gat_UA-56836576-1=1; __atuvc=22%7C10; __atuvs=5a9efe1bcb6123b9004'
      	}, 
		transform: function (body) {
			return cheerio.load(body);
		}
	}

	rp(options)
	.then(($) => {
	    //console.log($);
	    var imgLink = $('#bankImage').attr('src').split(';')[0];
	    var price = $('#lblOfferPrice .sp_amt').text(); 
	    console.log(imgLink, price);

		var item = new ItemModel ({
			title: data.title,
			description: data.description,
			link: data.link,
			imgLink: imgLink,
			price: Number(price),
			category: data.category
		})
		item.save(function (err) {
		    if (err) {
				console.log ('Error on save!', err); 
				//return res.status(500);
				done(err);
		    }
		    done();
	  	});

	})
	.catch((err) => {
		console.log(err);
		done(err);
	});

}

const rp = require('request-promise');
const cheerio = require('cheerio');
var mongoose = require ('mongoose');

var uristring = process.env.MONGODB_URI;

mongoose.connect(uristring, function (err, res) {
  if (err) { 
    console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + uristring);
  }
});

var itemSchema = new mongoose.Schema({
	title: { type: String, trim: true },
	description: { type: String},
	link: { type: String/*, unique: true*/},
	imgLink: { type: String/*, unique: true*/},
	price: { type: Number/*, unique: true*/},
	category: {type: String}
});

var ItemModel = mongoose.model('items', itemSchema);

